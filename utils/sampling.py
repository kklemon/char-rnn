import numpy as np


def sample_from_prediction(pred, diversity):
    pred = np.log(pred) * diversity
    pred = np.exp(pred)
    pred /= np.sum(pred) + 1e-4
    probas = np.random.multinomial(1, pred, 1)
    return np.argmax(probas)

def sample(model, text_processor,
           max_tokens=None, until_eos=False,
           diversity=1.0, refeed=False,
           initial_input=None):
    sos = text_processor.token_to_index[text_processor.ST_SOS]
    eos = text_processor.token_to_index[text_processor.ST_EOS]
    
    model_bs = model.input_shape[0]
    
    result = []
    
    if not initial_input:
        input = np.zeros([model_bs, 1], dtype='uint16')
        input[0, 0] = sos
    else:
        seq = text_processor.to_sequence([initial_input])[0]
        for token in seq[:-1]:
            input = np.zeros([model_bs, 1], dtype='uint16')
            input[0, 0] = token
            model.predict(input)
        input = np.zeros([model_bs, 1], dtype='uint16')
        input[0, 0] = seq[-1]
        
        result = seq.tolist()
       
    
    for i in range(max_tokens):
        pred = model.predict(input)[0][0]
        idx = sample_from_prediction(pred, diversity)
        
        if until_eos and idx == eos:
            break
        
        result.append(idx)
        
        input = np.zeros([model_bs, 1], dtype='uint16')
        input[0, 0] = idx
       
    tokens = text_processor.from_sequence([result])[0]
    
    return ''.join(tokens)