import math
import numpy as np
from keras.utils import Sequence
from .text_processor import TextProcessor

class TextSequence(Sequence):
    def __init__(self, text, seq_length, batch_size,
                 x_format='sparse', y_format='sparse'):
        self.text = text
        self.seq_length = seq_length
        self.batch_size = batch_size

        for name, value in zip(['x_format', 'y_format'], [x_format, y_format]):
            if value not in ['sparse', 'one-hot']:
                raise ValueError('%s argument must be either \'sparse\' or \'one-hot\' not \'%s\'' % (name, value))
        
        self.tp = TextProcessor()
        self.tp.fit(text)

        self.batches_x = []
        self.batches_y = []
        
        text_seq = self.tp.to_sequence([text], add_sos=True)[0]
        batch_cursors = np.random.randint(0, len(self), size=batch_size) * seq_length

        for i in range(len(self)):
            slices_x = [self.text[cursor : cursor + self.seq_length] for cursor in batch_cursors]
            slices_y = [self.text[cursor + 1 : cursor + self.seq_length + 1] for cursor in batch_cursors]

            batch_cursors = list(map(lambda x: (x + seq_length) % len(text), batch_cursors))

            seq_x = self.tp.to_sequence(slices_x, seq_length=seq_length)
            seq_y = self.tp.to_sequence(slices_y, seq_length=seq_length)

            batch_x = seq_x
            batch_y = seq_y

            if x_format == 'one_hot':
                batch_x = self.tp.to_one_hot(seq_x)
            
            if y_format == 'one_hot':
                batch_y = self.tp.to_one_hot(seq_y)
            else:
                batch_y = np.reshape(seq_y, seq_y.shape + (1, ))
            
            self.batches_x.append(batch_x)
            self.batches_y.append(batch_y)
    
    @staticmethod
    def load_from_file(filename, seq_length, batch_size, encoding=None):
        with open(filename, encoding=encoding) as fp:
            text = fp.read()
            return TextSequence(text, seq_length, batch_size)
    
    def shuffle_cursor(self):
        raise NotImplementedError

    def __len__(self):
        return math.ceil(len(self.text) / self.seq_length)
    
    def __getitem__(self, idx):
        return (
            self.batches_x[idx],
            self.batches_y[idx]
        )

