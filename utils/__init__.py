from .text_processor import TextProcessor
from .text_sequence import TextSequence
from .sampling import sample