import numpy as np
from collections import Counter
from itertools import chain

class TextProcessor(object):
    ST_PAD = '<pad>'
    ST_UKN = '<ukn>'
    ST_SOS = '<sos>'
    ST_EOS = '<eos>'

    def __init__(self):
        self.token_to_index = None
        self.index_to_token = None

        self.special_tokens = [self.ST_PAD, self.ST_UKN, self.ST_SOS, self.ST_EOS]
    
    def _add_token(self, token):
        self.token_to_index[token] = len(self.token_to_index)
        self.index_to_token[len(self.index_to_token)] = token

    def fit(self, data, max_vocab=None):
        if isinstance(data, str):
            data = [data]
        else:
            if not hasattr(data, '__iter__'):
                raise ValueError('\'data\' argument must be a list of iterables')
        
        self.token_to_index = {}
        self.index_to_token = {}

        for token in [self.ST_PAD, self.ST_UKN, self.ST_SOS, self.ST_EOS]:
            self._add_token(token)
        
        counter = Counter(chain.from_iterable(data))
        most_common = set([t[0] for t in counter.most_common(max_vocab)])
        for token in most_common:
            self._add_token(token)
    
    def to_sequence(self, data, seq_length=None, add_sos=False, add_eos=False):
        if seq_length is None:
            seq_length = max(map(len, data))
            if add_sos:
                seq_length += 1
            if add_eos:
                seq_length += 1
        
        target = np.zeros([len(data), seq_length], dtype='uint32')
        for i, seq in enumerate(data):
            start_index = 0
            if add_sos:
                target[i, start_index] = self.token_to_index[self.ST_SOS]
                start_index += 1

            idx_ukn = self.token_to_index[self.ST_UKN]
            for token in seq[:seq_length]:
                idx = self.token_to_index.get(token, idx_ukn)
                target[i, start_index] = idx
                start_index += 1
                if start_index >= seq_length:
                    break
            
            if add_eos:
                if start_index >= seq_length:
                    target[i, -1] = self.token_to_index[self.ST_EOS]
                else:
                    target[i, start_index] = self.token_to_index[self.ST_EOS]
        
        return target

    def to_one_hot(self, data, ignore_pad=True):
        if not isinstance(data, np.ndarray):
            raise ValueError('\'data\' must be of type numpy.ndarray')

        vec = np.zeros(data.shape + (self.vocab_size, ), dtype='float32')
        for i, seq in enumerate(data):
            for j, idx in enumerate(seq):
                if not ignore_pad or idx:
                    vec[i, j, idx] = 1.0
        
        return vec
    
    def from_one_hot(self, data):
        return np.argmax(data, axis=-1)
    
    def from_sequence(self, data, ignore_pad=True):
        results = []
        for seq in data:
            current = []
            for idx in seq:
                if ignore_pad and not idx:
                    break
                current.append(self.index_to_token.get(idx, self.ST_UKN))
            results.append(current)
        return results


    @property
    def vocab_size(self):
        return len(self.index_to_token)
