#!/usr/bin/env python3

import os
import random
import signal
import operator
import argparse

from functools import reduce
from keras.callbacks import Callback
from keras.optimizers import Adam

from utils import TextSequence, sample
from model import CharRNN


class GenerateSamplesCallback(Callback):
    def __init__(self,
                 inference_model,
                 data,
                 max_sampling_length=100,
                 sample_from=None,
                 temperatures=None):
        self.inference_model = inference_model
        self.data = data
        self.max_sampling_length = max_sampling_length
        self.sampling_text = sample_from
        self.temperatures = temperatures

    def on_epoch_end(self, epoch, logs={}):
        print()
        print('Copying weights...')

        self.inference_model.set_weights(self.model.get_weights())

        print('Generating samples after epoch %d' % (epoch + 1))

        temperatures = self.temperatures if self.temperatures else [1.0]
        for temp in temperatures:
            initial_input = None
            if self.sampling_text:
                initial_input = random.choice(self.sampling_text)
                print('# Temperature level of %f, sampling with \'%s\'' % (temp, initial_input))
            else:
                print('# Temperature level of %f' % temp)

            # FIXME: in some rare cases the probabilities passed to the numpy.random.multinomial function in sample 
            # might have a sum larger than one due to numeric errors which causes an exception to be thrown. 
            try:
                result = sample(self.inference_model, self.data.tp, self.max_sampling_length,
                                diversity=temp,
                                initial_input=initial_input)
                print(result)
            except:
                print('Error during inference')


class InterruptTrainingCallback(Callback):
    """Callback that stops training when one of the given signals is caught.

    # Arguments
        check_on_batch_end: whether to check if training should be stopped after every batch.
        check_on_epoch_end: whether to check if training should be stopped after every epoch.
        signals: a list of signals from the `signal` module.
    """
    def __init__(self,
                 check_on_batch_end=True,
                 check_on_epoch_end=True,
                 signals=[signal.SIGINT]):
        self.check_on_batch_end = check_on_batch_end
        self.check_on_epoch_end = check_on_epoch_end
        self.signals = signals
        self.stop = False

        if not signals:
            raise ValueError('signals argument must be a list of signals')
        
        signal_sum = reduce(operator.or_, signals)
        signal.signal(signal_sum, self.signal_handler)
    
    def signal_handler(self, signal, frame):
        self.stop = True

    def on_batch_end(self, batch, logs={}):
        if self.check_on_batch_end and self.stop:
            self.model.stop_training = True

    def on_epoch_end(self, epoch, logs={}):
        if self.check_on_epoch_end and self.stop:
            self.model.stop_training = True


def train(model,
          data_set,
          steps_per_epoch,
          epochs,
          inference_model=None,
          sample_from=None,
          verbose=1):
    callbacks = [InterruptTrainingCallback()]
    if inference_model:
        callbacks += [GenerateSamplesCallback(inference_model, data_set,
                                              sample_from=sample_from,
                                              temperatures=[0.2, 0.7, 1.0])]
    
    model.fit_generator(data_set,
                        steps_per_epoch=steps_per_epoch,
                        epochs=epochs,
                        callbacks=callbacks,
                        verbose=verbose)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a character based recurrent neural network (Char RNN) on a dataset.')
    parser.add_argument('data_set', type=str)
    parser.add_argument('--load-model', type=str, dest='load_model', default=None)
    parser.add_argument('--steps-per-epoch', type=int, dest='steps_per_epoch', default=None)
    parser.add_argument('--epochs', type=int, default=1)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--target-lr', type=float, dest='target_lr', default=None)
    parser.add_argument('--sample', type=bool, default=True)
    parser.add_argument('--sample-from', type=str, dest='sample_from', default=None)
    parser.add_argument('--save-model', type=str, dest='save_model', default='model.hd5')
    parser.add_argument('--seq-length', type=int, dest='seq_length', default=128)
    parser.add_argument('--batch-size', type=int, dest='batch_size', default=8)
    parser.add_argument('--embedding-size', type=int, dest='embedding_size', default=30)
    parser.add_argument('--rnn-dim', type=int, dest='rnn_dim', default=128)
    parser.add_argument('--rnn-layers', type=int, dest='rnn_layers', default=1)
    parser.add_argument('--verbose', type=int, default=1)
    args = parser.parse_args()

    if args.load_model is not None:
        raise NotImplementedError('The --load-model argument is not implemented yet')

    with open(args.data_set, encoding='utf8') as fp:
        data = TextSequence(fp.read(), args.seq_length, args.batch_size)

    if args.steps_per_epoch is not None:
        steps_per_epoch = args.steps_per_epoch
    else:
        steps_per_epoch = len(data)

    if args.target_lr is not None:
        decay = ((args.lr / args.target_lr) - 1) / (steps_per_epoch * args.epochs)
        print('Will use an initial learning rate of {:.4f} with a decay of {:.4f} targeting {:.5f} after \
              {} iterations'.format(args.lr, decay, args.target_lr, args.epochs * steps_per_epoch))
    else:
        decay = 0.0
        print('Will use a learning rate of {:.4f}'.format(args.lr))

    optimizer = Adam(lr=args.lr, decay=decay)

    model_input_shape = (args.batch_size, None)
    inference_input_shape = (1, None)

    model_parameters = {
        'vocab_size': data.tp.vocab_size,
        'embedding_size': args.embedding_size,
        'recurrent_dim': args.rnn_dim,
        'recurrent_layers': args.rnn_layers,
        'stateful': True
    }

    print('Building models...')

    model = CharRNN(**{**model_parameters,
                       'input_shape': model_input_shape})
    model.compile(optimizer=optimizer,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    
    inference_model = CharRNN(**{**model_parameters,
                                 'input_shape': inference_input_shape})
    
    if args.sample:
        if args.sample_from:
            with open(args.sample_from, encoding='utf8') as fp:
                sampling_list = [line.strip() for line in fp if line.strip()]
    else:
        inference_model = None
        sampling_list = None

    print('Starting training...')

    train(model,
          data,
          steps_per_epoch,
          args.epochs,
          inference_model if args.sample else None,
          sampling_list,
          args.verbose)
    
    if args.save_model:
        filename, ext = os.path.splitext(args.save_model)

        model_filename = args.save_model
        model_weights_filename = filename + '_weights' + ext

        inference_filename = filename + '_inference' + ext

        model.save(model_filename)
        model.save_weights(model_weights_filename)

        # TODO: fix sampling (character dictionary needs to be handled aswell)
        # inference_model.save(inference_filename)

        print('Model has been saved under \'{}\''.format(args.save_model))

    