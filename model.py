from keras.models import Sequential
from keras.layers import *


def CharRNN(input_shape,
            vocab_size,
            embedding_size,
            recurrent_dim,
            recurrent_layers=1,
            embedding_weights=None,
            stateful=False,
            mask_zero=True,
            final_activation='softmax'):
    seq = Sequential()

    if stateful:
        seq.add(InputLayer(batch_input_shape=input_shape))
    else:
        seq.add(InputLayer(input_shape=input_shape))

    seq.add(Embedding(vocab_size, embedding_size, weights=embedding_weights, mask_zero=mask_zero))
    
    for i in range(recurrent_layers):
        seq.add(LSTM(recurrent_dim, stateful=stateful, return_sequences=True))
    
    seq.add(TimeDistributed(Dense(vocab_size)))
    seq.add(Activation('softmax'))
    
    return seq
    